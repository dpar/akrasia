(ns akrasia.dailies
  (:require [java-time :as time])
  (:use [akrasia.runtime-state]))


;; An hash-map of Daily Tasks is
(comment
  (def dl-example {1 {:after (time/local-time 8 30)
                      :text "d1"}
                   2 {:after (time/local-time 10 0)
                      :text "d2"
                      :last-checked (time/local-date 2018 11 5)}}))
;; The hash key is the id of the task,
;; the hash value contains an hash-map where
;; - :after represents the time of day after which the task should be presented
;;   to the user
;; - :last-checked is a date representing the last day the user has
;;   checked the task

(defn add-new! [text h m]
  (swap! dailies
         assoc
         (.getEpochSecond (time/instant))
         {:text  text
          :after (time/local-time h m)}))


(defn edit! [tid text h m]
  (let [task (@dailies tid)
        mtask (assoc task
                     :text  text
                     :after (time/local-time h m))]
    (swap! dailies
           assoc-in
           [tid]
           mtask)))

(defn delete-by-id! [id]
  (swap! dailies dissoc id))

(defn set-checked-by-id! [id]
  (swap! dailies
         assoc-in
         [id :last-checked]
         (time/local-date)))

(defn dt-due?
  "produces true if dt :last-checked is not today and dt :after is before the current time"
  [now-date-time dt]
  (let [now (time/local-time now-date-time)
        today (time/local-date now-date-time)]
    (and (let [lc (:last-checked dt)]
           (or (nil? lc)
               (time/before? lc
                             today)))
         (time/after? now
                      (:after dt)))))

(defn get-dailies-due [dls]
  (filter #((partial dt-due?
                     (time/local-date-time)) (get % 1))
          dls))
