(ns akrasia.to-dos
  (:require [java-time :as time])
  (:use     [akrasia.runtime-state]))

;; An hash-map of To-dos is
(def to-dos-example
  {1 {:text "to-dos 1 text"
      :completed false}
   2 {:text "to-dos 2 text"
      :completed true}})
;; The hash key is the id of the to-do

(defn add-new! [text]
  (swap! to-dos
         assoc
         (.getEpochSecond (time/instant))
         {:text  text
          :completed false}))

(defn edit! [id text compl]
  (let [to-do  (@to-dos id)
        mto-do (assoc to-do
                      :text text
                      :completed compl)]
    (swap! to-dos
           assoc-in
           [id]
           mto-do)))

(defn set-completed-by-id! [id]
  (swap! to-dos
         assoc-in
         [id :completed]
         true))

(defn clear-all-completed! []
  (swap! to-dos
         (fn [ts]
           (into {}
                 (filter #(not (:completed (get % 1))) ts)))))

(defn delete-by-id! [id]
  (swap! to-dos
         dissoc
         id))
