(ns akrasia.persistence
  (:require [clojure.data.json :as json]
            [clojure.java.io :as io]
            [clojure.edn :as edn]
            [java-time :as time]
            [environ.core :refer [env]]))

#_(defn init-persistence! []
    (reset! persistence (load-persistence)))

#_(def persistence (atom nil))

#_(def allowed-persistence-tags
    [:dailies])

#_(add-watch persistence
             :debug-text
             (fn [_ _ old new]
               (println new)))

#_(defn query-persistence [key]
    (if-not (some #(= key %) allowed-persistence-tags)
      (throw (Exception. "Key not allowed")))
    (key @persistence :absent))

#_(defn save-persistence! [key value]
    (if-not (some #(= key %) allowed-persistence-tags)
      (throw (Exception. "Key not allowed")))
    (swap! persistence assoc key value))

(defn serialize-dailies [dailies]
  (let [ser-task (fn [task]
                   (let [id (get task 0)
                         t (get task 1)
                         aft (:after t)
                         lc (:last-checked t)
                         out (let [taft (assoc t
                                               :after
                                               [(.getHour aft)
                                                (.getMinute aft)])]
                               (if lc
                                 (assoc
                                  taft
                                  :last-checked
                                  (time/as lc :year :month-of-year :day-of-month))
                                 taft))]
                     [id out]))]
    (into {} (map ser-task dailies))))


(defn deserialize-dailies [dailies]
  (let [des-task (fn [task]
                   (let [id (get task 0)
                         t (get task 1)
                         aft (:after t)
                         lc (:last-checked t)
                         out (let [taft (assoc t
                                               :after
                                               (apply time/local-time aft))]
                               (if lc
                                 (assoc
                                  taft
                                  :last-checked
                                  (apply time/local-date lc))
                                 taft))]
                     [id out]))]
    (into {} (map des-task dailies))))

; System.out.println(System.getProperty("java.io.tmpdir"));

(defn get-dir []
  (let [dir (if (not= "true" (env :dev?))
              (io/file (System/getProperty "user.home")
                       ".config"
                       "akrasia")
              (io/file (System/getProperty "java.io.tmpdir")
                       "akrasia_dev"))]
    (if (.exists dir)
      dir
      (do (.mkdirs dir)
          dir))))

(defn load-persistence-file [filename]
  (let [p-file (io/file (get-dir) filename)]
    (if (.exists p-file)
      (slurp p-file)
      false)))

(defn load-config []
  (if-let [pf (load-persistence-file "config.edn")]
    (let [cf (edn/read-string pf)]
      (if (map? cf)
        cf
        (do (println "Malformed configuration file")
            (System/exit 0))))
    false))


(defn load-to-dos []
  (if-let [pf (load-persistence-file "to_dos.edn")]
    (edn/read-string pf)
    {}))

(defn save-to-dos [value]
  (let [p-file (io/file (get-dir) "to_dos.edn")]
    (spit p-file (pr-str value))))

(defn load-dailies []
  (if-let [pf (load-persistence-file "dailies.edn")]
    (deserialize-dailies (edn/read-string pf))
    {}))

(defn save-dailies [value]
  (let [p-file (io/file (get-dir) "dailies.edn")]
    (spit p-file (pr-str (serialize-dailies value)))))
