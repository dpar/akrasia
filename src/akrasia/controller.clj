(ns akrasia.controller
  (:require [overtone.at-at :as at]
            [akrasia.ui :as ui]
            [akrasia.utils :as utils])
  (:import  [javafx.event ActionEvent])
  (:use     [akrasia.runtime-state :only [runtime-state
                                          akrasia-state]])
  (:gen-class
   :methods [[request2 [javafx.event.ActionEvent] void]
             [request5 [javafx.event.ActionEvent] void]
             [request10 [javafx.event.ActionEvent] void]
             [request25 [javafx.event.ActionEvent] void]
             [closeScreen [javafx.event.ActionEvent] void]
             [dailiesDeleteHandler [javafx.event.ActionEvent] void]
             [dailiesNewHandler [javafx.event.ActionEvent] void]
             [dailiesPushHandler [javafx.event.ActionEvent] void]
             [to_dosDeleteHandler [javafx.event.ActionEvent] void]
             [to_dosPushHandler [javafx.event.ActionEvent] void]
             [to_dosClearAllHandler [javafx.event.ActionEvent] void]
             [to_dosNewHandler [javafx.event.ActionEvent] void]]))

(defn open-screen-for
  [min]
  (when-let [job (:screen-job @akrasia-state)]
    (at/stop job))
  (let [job (at/after (utils/min->ms min)
                      #(swap! akrasia-state
                              assoc
                              :screen-status :closed
                              :screen-job nil)
                      (:thread-pool @runtime-state))]
    (swap! akrasia-state
           assoc
           :screen-status :open
           :screen-job job
           :duration min)))

(defn -request2 [this ^ActionEvent event]
  (open-screen-for 2))

(defn -request5 [this ^ActionEvent event]
  (open-screen-for 5))

(defn -request10 [this ^ActionEvent event]
  (open-screen-for 10))

(defn -request25 [this ^ActionEvent event]
  (open-screen-for 25))

(defn -closeScreen [this ^ActionEvent event]
  (when-let [job (:screen-job @akrasia-state)]
    (at/stop job))
  (swap! akrasia-state
         assoc
         :screen-status :closed
         :screen-job nil))

(defn -dailiesNewHandler [this ^ActionEvent event]
  (ui/dailies-on-new))

(defn -dailiesPushHandler [this ^ActionEvent event]
  (ui/dailies-on-push))

(defn -dailiesDeleteHandler [this ^ActionEvent event]
  (ui/dailies-on-delete))

(defn -to_dosDeleteHandler [this ^ActionEvent event]
  (ui/to-dos-on-delete))

(defn -to_dosPushHandler [this ^ActionEvent event]
  (ui/to-dos-on-push))

(defn -to_dosClearAllHandler [this ^ActionEvent event]
  (ui/to-dos-on-clear-all))

(defn -to_dosNewHandler [this ^ActionEvent event]
  (ui/to-dos-on-new))

