(ns akrasia.runtime-state
  (:require [overtone.at-at :as at]
            [akrasia.persistence :as p]
            [akrasia.utils :as u]
            [environ.core :refer [env]]
            [java-time :as time])
  (:use [clojure.java.shell :only [sh]]))

(def config (atom {:on-open-command false
                   :on-close-command false}))

(def runtime-state (atom {}))
(def dailies (atom nil))
(def to-dos (atom nil))
(def akrasia-state (atom nil))

(def stage-title (if (= "true" (env :dev?))
                   "Akrasiadev"
                   "Akrasia"))

(defn handle-akrasia-state
  [_ _ old new]
  (println (time/local-time) " : " new)
  (case (:screen-status new)
    :closed
    (do
      (u/javafx-run-later
       (fn []
         (doto (@runtime-state :stage)
           (.setAlwaysOnTop true)
           (.setIconified false)
           (.setMaximized true)
           .show
           #_.centerOnScreen)))
      (if (= (System/getProperty "os.name")
             "Linux")
        (u/focus-stage stage-title))
      (when-let [occ (@config :on-close-command)]
        (future (println (sh occ)))))
    :open
    (do
      (u/javafx-run-later
       (fn []
         (doto (@runtime-state :stage)
           (.setAlwaysOnTop false)
           #_(.setIconified true))))
      (when-let [ooc (@config :on-open-command)]
        (future (println (sh ooc "-d" (str (:duration new)))))))))

(defn init-runtime-state! [scene stage]
  (reset! runtime-state {:thread-pool (at/mk-pool)
                         :scene scene
                         :stage stage})
  ;; temp
  (reset! to-dos
          (p/load-to-dos))
  (reset! dailies
          (p/load-dailies))
  (when-let [conf (p/load-config)]
    (swap! config into conf))
  (add-watch akrasia-state
             nil
             handle-akrasia-state)
  ;; I want the watch to be triggered
  (reset! akrasia-state {:screen-status :closed})
  (add-watch dailies
             :save
             (fn [_ _ old new]
               (p/save-dailies new)))
  (add-watch to-dos
             :save
             (fn [_ _ old new]
               (p/save-to-dos new))) )
