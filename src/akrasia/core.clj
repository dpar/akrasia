(ns akrasia.core
  (:require [clojure.java.io :as io]
            [akrasia.persistence :as ps]
            [akrasia.ui :as ui]
            [akrasia.dailies :as dailies]
            [akrasia.to-dos :as to-dos]
            [akrasia.utils :as utils])
  (:use     [akrasia.runtime-state]
            [clojure.java.shell :only [sh]])
  (:import  [javafx.application Application]
            [javafx.scene Scene]
            [javafx.stage Stage]
            [javafx.fxml FXMLLoader]
            [javafx.scene.control CheckBox]
            [javafx.scene.layout StackPane])
  (:gen-class :extends javafx.application.Application))


(defn -main
  [& args]
  (Application/launch akrasia.core (into-array String args)))


(defn -start [^akrasia.core app ^Stage stage]
  (let [root (FXMLLoader/load (io/resource "main.fxml"))
        scene (Scene. root 640 400)]
    (doto (-> scene
              .getStylesheets)
      (.add "bootstrap3.css")
      (.add "app.css"))
    (init-runtime-state! scene stage)
    (ui/init-ui!)

    (-> stage
        (.setOnCloseRequest (utils/event-handler (fn [e]
                                                   (javafx.application.Platform/exit)
                                                   (System/exit 0)))))
    (if (= (System/getProperty "os.name")
           "Linux")
      (-> stage
          .focusedProperty
          (.addListener (reify javafx.beans.value.ChangeListener
                          (changed [this observable old-value new-value]
                            (if (not new-value)
                              (if (= :closed (@akrasia-state :screen-status))
                                (future
                                  (Thread/sleep 3000)
                                  (utils/focus-stage stage-title)))))))))
    (-> stage
        .iconifiedProperty
        (.addListener (reify javafx.beans.value.ChangeListener
                        (changed [_ _ _ _]
                          (if (= :closed (@akrasia-state :screen-status))
                            (utils/javafx-run-later #(.setIconified stage false)))))))
    (-> stage
        .maximizedProperty
        (.addListener (reify javafx.beans.value.ChangeListener
                        (changed [_ _ _ _]
                          (if (= :closed (@akrasia-state :screen-status))
                            (utils/javafx-run-later #(.setMaximized stage true)))))))
    (doto stage
      (.setMaximized true)
      #_(.setResizable false)
      (.setTitle stage-title)
      (.setMinWidth 640)
      (.setMinHeight 400)
      (.setScene scene)
      .show)))
