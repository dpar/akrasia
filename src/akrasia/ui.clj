(ns akrasia.ui
  (:require [akrasia.utils :as u]
            [akrasia.dailies :as dailies]
            [akrasia.to-dos :as to-dos]
            [java-time :as time]
            [overtone.at-at :as at])

  (:use     [akrasia.runtime-state])
  (:import  [javafx.scene.control CheckBox]
            [javafx.scene.control Label]
            [javafx.scene.control SpinnerValueFactory$IntegerSpinnerValueFactory]))


(def dailies-ui-state (atom {:action :new}))
(def to-dos-ui-state  (atom {:action :new}))

(defn make-checkbox [id text action]
  (let [cb   (CheckBox.)]
    (doto cb
      (.setWrapText true)
      (.setText text)
      (.setUserData {:id id})
      (.setOnAction action))))

(defn redraw-main-dailies-pane! [dailies-due]
  (u/javafx-run-later
   (fn []
     (let [dp (-> (:scene @runtime-state)
                  (.lookup "#main-dailies-pane"))]
       (-> dp
           .getChildren
           .clear)
       (doseq [d dailies-due]
         (let [id   (get d 0)
               text (:text (get d 1))
               action (u/event-handler
                       (fn [e]
                         (-> e
                             .getSource
                             .getUserData
                             :id
                             dailies/set-checked-by-id!)))]
           (-> dp
               .getChildren
               (.add (make-checkbox id text action)))))))))

(defn redraw-main-to-dos-pane! [to-dos]
  (u/javafx-run-later
   (fn []
     (let [tp (-> (:scene @runtime-state)
                  (.lookup "#main-to-dos-pane"))
           comp-to-dos (filter #(not (:completed (get % 1))) to-dos)]
       (-> tp
           .getChildren
           .clear)
       (doseq [t comp-to-dos]
         (let [id   (get t 0)
               text (:text (get t 1))
               action (u/event-handler
                       (fn [e]
                         (-> e
                             .getSource
                             .getUserData
                             :id
                             to-dos/set-completed-by-id!)))]
           (-> tp
               .getChildren
               (.add (make-checkbox id text action)))))))))

(defn redraw-dailies-form! []
  (u/javafx-run-later
   (fn []
     (let [scene (:scene @runtime-state)
           del   (.lookup scene "#dailies-delete-button")
           new   (.lookup scene "#dailies-new-button")
           tf    (.lookup scene "#dailies-text-field")
           hf    (.lookup scene "#dailies-hour-field")
           mf    (.lookup scene "#dailies-minute-field")
           hsvf  (SpinnerValueFactory$IntegerSpinnerValueFactory. 0 24 12)
           msvf  (SpinnerValueFactory$IntegerSpinnerValueFactory. 0 55 0 5)
           uist  @dailies-ui-state]
       (.setValueFactory hf hsvf)
       (.setValueFactory mf msvf)
       (cond
         (=  (:action uist) :new)
         (do (.setDisable new true)
             (.setDisable del true)
             (.setText tf ""))
         (=  (:action uist) :edit)
         (let [tid  (uist :task-id)
               task (@dailies tid)]
           (.setDisable new false)
           (.setDisable del false)
           (.setText tf (:text task))
           (.setValue hsvf (.getHour (:after task)))
           (.setValue msvf (.getMinute (:after task)))))))))

(defn redraw-to-dos-form! []
  (u/javafx-run-later
   (fn []
     (let [scene (:scene @runtime-state)
           del   (.lookup scene "#to-dos-delete-button")
           new   (.lookup scene "#to-dos-new-button")
           tf    (.lookup scene "#to-dos-text-field")
           ccb   (.lookup scene "#to-dos-completed-checkbox")
           uist  @to-dos-ui-state]
       (cond
         (=  (:action uist) :new)
         (do (.setDisable new true)
             (.setDisable del true)
             (.setText tf "")
             (.setSelected ccb false))
         (=  (:action uist) :edit)
         (let [tid  (uist :to-do-id)
               to-do (@to-dos tid)]
           (.setDisable new false)
           (.setDisable del false)
           (.setText tf (:text to-do))
           (.setSelected ccb (:completed to-do))))))))

(defn sort-dailies [dailies]
  (sort-by (comp :after second) dailies))

(defn redraw-dailies-listview! [dailies]
  (u/javafx-run-later
   (fn []
     (let [scene (:scene @runtime-state)
           items (-> scene
                     (.lookup "#dailies-listview")
                     .getItems)]
       (.clear items)
       (doseq [d (sort-dailies dailies)]
         (let [id (get d 0)
               t  (get d 1)
               lb (Label. (str (time/format "HH:mm"
                                            (:after t))
                               " - "
                               (:text t)))]
           (doto lb
             (.setUserData {:id id}))
           (when-let [lc (:last-checked t)]
             (when (= lc (time/local-date))
               (-> lb
                   .getStyleClass
                   (.add "task-checked"))))
           (.add items lb)))))))

(defn redraw-to-dos-listview! [to-dos]
  (u/javafx-run-later
   (fn []
     (let [scene (:scene @runtime-state)
           items (-> scene
                     (.lookup "#to-dos-listview")
                     .getItems)]
       (.clear items)
       (doseq [t to-dos]
         (let [id (get t 0)
               t  (get t 1)
               lb (Label. (str (:text t)))]
           (doto lb
             (.setUserData {:id id}))
           (.add items lb)))))))

(defn to-dos-on-new []
  (reset! to-dos-ui-state {:action :new}))

(defn to-dos-on-delete []
  (to-dos/delete-by-id! (:to-do-id @to-dos-ui-state)))

(defn to-dos-on-push[]
  (let [scene (:scene @runtime-state)
        text (-> (.lookup scene "#to-dos-text-field")
                 .getText)
        compl (-> (.lookup scene "#to-dos-completed-checkbox")
                 .isSelected)
        uist @to-dos-ui-state
        act (:action uist)
        tid (:to-do-id uist)]
    (cond
      (= :new act)
      (to-dos/add-new! text)
      (= :edit act)
      (to-dos/edit! tid text compl))))

(defn to-dos-on-clear-all []
  (to-dos/clear-all-completed!))

(defn dailies-on-new []
  (reset! dailies-ui-state {:action :new}))

(defn dailies-on-delete []
  (dailies/delete-by-id! (:task-id @dailies-ui-state)))

(defn dailies-on-push[]
  (let [scene (:scene @runtime-state)
        text (-> (.lookup scene "#dailies-text-field")
                 .getText)
        h (-> (.lookup scene "#dailies-hour-field")
              .getValueFactory
              .getValue)
        m (-> (.lookup scene "#dailies-minute-field")
              .getValueFactory
              .getValue)
        uist @dailies-ui-state
        act (:action uist)
        tid (:task-id uist)]
    (cond
      (= :new act)
      (dailies/add-new! text h m)
      (= :edit act)
      (dailies/edit! tid text h m))))

(defn set-dailies-listview-handler! []
  (-> (:scene @runtime-state)
      (.lookup "#dailies-listview")
      .getSelectionModel
      .selectedItemProperty
      (.addListener (reify javafx.beans.value.ChangeListener
                      (changed [_ _ _ new]
                        (when new
                          (reset! dailies-ui-state
                                  {:action :edit
                                   :task-id (:id (.getUserData new))})))))))

(defn set-to-dos-listview-handler! []
  (-> (:scene @runtime-state)
      (.lookup "#to-dos-listview")
      .getSelectionModel
      .selectedItemProperty
      (.addListener (reify javafx.beans.value.ChangeListener
                      (changed [_ _ _ new]
                        (when new
                          (reset! to-dos-ui-state
                                  {:action :edit
                                   :to-do-id (:id (.getUserData new))})))))))

(defn init-dailies-ui! []
  ;; check for due dailies every 5 minutes
  (at/every (u/min->ms 5)
            (fn []
              (redraw-main-dailies-pane! (dailies/get-dailies-due @dailies)))
            (@runtime-state :thread-pool)
            :initial-delay (u/min->ms 5))
  (add-watch dailies-ui-state
             nil
             (fn [_ _ _ new]
               (redraw-dailies-form!)))
  (add-watch dailies
             :dailies
             (fn [_ _ old new]
               (redraw-main-dailies-pane! (dailies/get-dailies-due new))
               (redraw-dailies-listview! new)
               ;; check if a task has been deleted
               (if (not= (count new) (count old))
                 (reset! dailies-ui-state {:action :new}))))
  (set-dailies-listview-handler!)
  (redraw-main-dailies-pane! (dailies/get-dailies-due @dailies))
  (redraw-dailies-listview! @dailies)
  (redraw-dailies-form!))

(defn init-to-dos-ui! []
  (redraw-main-to-dos-pane! @to-dos)
  (redraw-to-dos-listview! @to-dos)
  (set-to-dos-listview-handler!)
  (redraw-to-dos-form!)
  (add-watch to-dos
             :to-dos
             (fn [_ _ old new]
               (redraw-main-to-dos-pane! @to-dos)
               (redraw-to-dos-listview! @to-dos)
               ;; check if a task has been deleted
               (if (not= (count new) (count old))
                 (reset! to-dos-ui-state {:action :new}))))
  (add-watch to-dos-ui-state
             nil
             (fn [_ _ _ new]
               (redraw-to-dos-form!))))

(defn init-ui! []
  (init-dailies-ui!)
  (init-to-dos-ui!))
