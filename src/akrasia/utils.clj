(ns akrasia.utils
  (use [clojure.java.shell :only [sh]]))

(defn event-handler [f]
  (reify javafx.event.EventHandler
    (handle [this event] (f event))))

(defn min->ms [min]
  (* min 1000 60))


(defn javafx-run-later [f]
  (javafx.application.Platform/runLater
   (reify Runnable
     (run [this]
       (f)))))

(defn focus-stage [title]
  (sh "wmctrl" "-F" "-a" title))

