(defproject akrasia "0.1.0"
  :description "Time boxed computing and personal organizer"
  :url "https://gitlab.com/dpar/akrasia"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [overtone/at-at "1.2.0"]
                 [org.clojure/data.json "0.2.6"]
                 [clojure.java-time "0.3.1"]
                 [environ "1.1.0"]]
  :plugins [[lein-environ "1.1.0"]]
  :main ^:skip-aot akrasia.core
  :aot [akrasia.core akrasia.controller]
  :target-path "target/%s"
  ;; :profiles {:uberjar {:aot :all}}
  )

