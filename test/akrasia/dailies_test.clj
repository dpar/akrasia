(ns akrasia.dailies-test
  (:require [akrasia.dailies :refer :all]
            [java-time :as t]
            [clojure.test :refer :all]))


(def test-date-time (t/local-date-time 2017 12 21))

(deftest due-test
  (is (dt-due? (t/adjust test-date-time (t/local-time 9))
               {:id 1
                :after (t/local-time 8 30)
                :text "d1"}))
  (is (not (dt-due? (t/adjust test-date-time (t/local-time 7))
                    {:id 1
                     :after (t/local-time 8 30)
                     :text "d1"})))
  (is (dt-due? (t/adjust test-date-time (t/local-time 10))
               {:id 1
                :after (t/local-time 8 30)
                :text "d1"
                :last-checked (t/local-date 2017 1 20)}))
  (is (not (dt-due? (t/adjust test-date-time (t/local-time 10))
                    {:id 1
                     :after (t/local-time 8 30)
                     :text "d1"
                     :last-checked (t/local-date test-date-time)}))))

